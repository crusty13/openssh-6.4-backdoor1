#!/usr/bin/env bash

URL_SSH="https://ftp.nluug.nl/security/OpenSSH/openssh-6.4p1.tar.gz"
URL_SSL="http://www.openssl.org/source/openssl-1.0.1e.tar.gz"
SSH_DIR=$(basename $URL_SSH .tar.gz)
SSL_DIR=$(basename $URL_SSL .tar.gz)

echo "Downloading necessary archives"
wget $URL_SSH $URL_SSL \
    && for file in *.tar.gz; do tar -zxf $file; done

find assets/ -maxdepth 1 -type f -exec cp {} $SSH_DIR \;

echo "Compiling custom openssh"
(cd $SSH_DIR && ./configure --with-ssl-dir=../openssl-1.0.1e/ --without-openssl-header-check \
    && make && make install)

echo "Copying sshd config"
cp assets/sshd/sshd_config /usr/local/etc/sshd_config

echo "Ready!"
